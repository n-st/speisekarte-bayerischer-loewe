OneLoewe: Parse Bayerischer Löwe online menu and try to guess which weekday corresponds to which exact date
===========================================================================================================

[Mittagsrenner](http://www.wirtshaus-passau.de/wochenkarte.php) only mentions
weekdays (Monday through Friday), but not the actual *date*.

This script tries to match the "today" section from the [main
page](http://www.wirtshaus-passau.de/index.php) with the full list, and
interpolate the actual dates based on that.

Output is currently separated by tab characters, so can easily be prettified using the `column` Unix coreutil.

Usage
-----

```
./OneLoewe.py | column -ts$'\t'
```

Example output:

```
Mo, 20.03.2017  Wochenrenner: Bunter Rindfleisch-Nudelsuppentopf  € 6,90
Di, 21.03.2017  Wochenrenner: Bunter Rindfleisch-Nudelsuppentopf  € 6,90
Di, 21.03.2017  Wochenrenner: Bunter Rindfleisch-Nudelsuppentopf  € 6,90
Mi, 22.03.2017  Wochenrenner: Bunter Rindfleisch-Nudelsuppentopf  € 6,90
Do, 23.03.2017  Wochenrenner: Bunter Rindfleisch-Nudelsuppentopf  € 6,90
Fr, 24.03.2017  Wochenrenner: Bunter Rindfleisch-Nudelsuppentopf  € 6,90
Mo, 27.03.2017  Deftiges Bauerngröstl                             € 6,90
Di, 28.03.2017  Puten Rahm Geschnetzeltes                         € 6,90
Mi, 29.03.2017  Leberkäs                                          € 6,90
Do, 30.03.2017  Pfannkuchen Mexiko                                € 6,90
Fr, 31.03.2017  Kap Seehecht vom Grill                            € 6,90
```

Note that that duplicate (Di, 21.3.) exists on the source website. Not my fault!
