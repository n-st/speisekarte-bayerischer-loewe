#!/usr/bin/env python2

# coding: utf-8

import sys
import codecs
sys.stdout = codecs.getwriter('utf8')(sys.stdout)

# In[73]:

import requests
from bs4 import BeautifulSoup
import datetime
import dateutil.parser as dateparser


# In[79]:

import locale
locale.setlocale(locale.LC_TIME, 'de_DE.UTF-8')


# In[57]:

mainpage = requests.get('http://www.wirtshaus-passau.de/index.php')
mainpage.raise_for_status()
weekmenu = requests.get('http://www.wirtshaus-passau.de/wochenkarte.php')
weekmenu.raise_for_status()


# In[66]:

mainsoup = BeautifulSoup(mainpage.text.replace('<br>', ''), 'html.parser')
weeksoup = BeautifulSoup(weekmenu.text.replace('<br>', ''), 'html.parser')

todaydate = mainsoup.find(id='mittag').find_all('span', attrs={'class': 'rot'})
weeks = weeksoup.find(id='speisen')


# In[59]:


# In[84]:

plan = []
for day in weeks.find_all('tr'):
    cells = day.find_all('td')
    if len(cells) != 5:
        # this is a separator line, not a menu item
        continue
    dow, course, name, sides, price = map(lambda x: x.string, cells)
    plan += [{'dow': dow, 'date': None, 'name': name, 'sides': sides, 'price': price}]
    
plan


# In[99]:

weekdays = {
    'Mo': 1,
    'Di': 2,
    'Mi': 3,
    'Do': 4,
    'Fr': 5,
    'Sa': 6,
    'So': 7
    }


# In[117]:

datestr = todaydate[0].string
foodstr = todaydate[0].find_next_sibling('b', attrs={'class': 'weissG'}).string

dateunix = datetime.datetime.strptime(datestr, '%A, den %d.%m.%Y')

matched_idx = None
for idx, day in enumerate(plan):
    if day['dow'] == dateunix.strftime('%a') and day['name'] == foodstr:
        day['date'] = dateunix
        matched_idx = idx
        
if matched_idx is None:
    raise Exception('No match found')

current_date = dateunix
current_weekday = datetime.datetime.strftime(dateunix, '%a')

for day in plan[matched_idx::-1]:
    current_date -= datetime.timedelta(days=(weekdays[current_weekday] - weekdays[day['dow']])%7)
    current_weekday = day['dow']
    day['date'] = current_date

current_date = dateunix
current_weekday = datetime.datetime.strftime(dateunix, '%a')

for day in plan[matched_idx:]:
    current_date += datetime.timedelta(days=(weekdays[day['dow']] - weekdays[current_weekday])%7)
    current_weekday = day['dow']
    day['date'] = current_date


# In[121]:

for day in plan:
    print(
        day['date'].strftime('%a, %d.%m.%Y') +
        '\t' +
        day['name'] +
        '\t' +
        #day['sides'] +
        #'\t' +
        day['price']
    )

